<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Order_Detail;
use App\Models\Transaction;
use App\Models\Menu;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function() use ($request){
            $orders = new Order;
            $orders->customer_name = $request->customer_name;
            $orders->driver_name = $request->driver_name;
            $orders->save();
            $order_id = $orders->id;

            for ($menu_id=0; $menu_id < count ($request->menu_id); $menu_id++) { 
                $order_details = new Order_Detail;
                $order_details->order_id = $order_id;
                $order_details->menu_id = $request->menu_id;
                $order_details->unit_price = $request->unit_price[$menu_id];
                $order_details->qty = $request->qty[$menu_id];
                $order_details->discount = $request->discount[$menu_id];
                $order_details->amount = $request->amount[$menu_id];
                $order_details->save();
            }

                $transaction = new Transaction;
                $transaction->order_id = $order_id;
                $transaction->user_id = auth()->user()->id;
                $transaction->balance = $request->balance;
                $transaction->paid_amount = $request->paid_amount;
                $transaction->save();


                $menus = Menu::all();
                $order_details = Order_Detail::where('order_id', $order_id)->get();
                $orderBy = Order::where('id', $order_id)->get();
        });

        return response()->json(Response::HTTP_UNPROCESSABLE_ENTITY);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
