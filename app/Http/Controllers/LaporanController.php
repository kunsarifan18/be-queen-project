<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Laporan;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $transaction = Order::with('transaction', 'order', 'menu')->get();

        $transaction = Transaction::select(
                                            "transactions.id",
                                            "transactions.order_id",
                                            "transactions.paid_amount",
                                            "transactions.type",
                                            "transactions.time",
                                            "orders.customer_name", 
                                            "orders.driver_name",
                                            "order_details.*")
                                            ->join("orders", "orders.id", "=", "transactions.order_id")
                                            ->join("order_details", "order_details.id", "=", "transactions.order_id")
                                            // ->join("menus", "menus.id", "=", "order_details.menu.id")
                                            ->get();
        $menu = DB::table('menus')
                ->join('order_details', 'order_details.menu_id', '=', 'menus.id')
                ->select('menus.name')
                ->get();
        $response = [
            'message' => 'List transaction order by time',
            'data' => $transaction,$menu
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function filterDate()
    {    
        if (request()->startDate || request()->endDate) {
                $startDate = Carbon::parse(request()->startDate)->toDateTimeString();
                $endDate = Carbon::parse(request()->endDate)->toDateTimeString();
                $filterDate = Transaction::whereBetween('time', [$startDate, $endDate])->get();
                $response = [
                    'message' => 'Laporan transaction order by date',
                    'data' => $filterDate
                ];

                return response()->json($response, Response::HTTP_OK);
            } else {
                $response = [
                    'message' => 'Not found',
                    'data' => $filterDate
                ];
                return response()->json($response, Response::HTTP_FAILED);
            }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Laporan  $laporan
     * @return \Illuminate\Http\Response
     */
    public function show(Laporan $laporan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Laporan  $laporan
     * @return \Illuminate\Http\Response
     */
    public function edit(Laporan $laporan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Laporan  $laporan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Laporan $laporan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Laporan  $laporan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Laporan $laporan)
    {
        //
    }
}
