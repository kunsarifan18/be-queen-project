<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Transaction;
use App\Models\OrderDetail;
use App\Models\Order;
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $transaction = Order::with('transaction', 'order', 'menu')->get();

        $transaction = DB::table('orders')
                        ->join('transactions','transactions.id', '=', 'orders.id')
                        ->join('order_details','orders.id', '=', 'order_details.order_id')
                        ->select('orders.customer_name','orders.driver_name','transactions.type','transactions.paid_amount','transactions.time', 'order_details.*')
                        // ->where('order_details','menus.id', '=', 'order_details.menu_id')

                        ->get();
        $menu = DB::table('menus')
                ->join('order_details', 'order_details.menu_id', '=', 'menus.id')
                ->select('menus.*')
                ->get();
        $response = [
            'message' => 'List transaction order by time',
            'data' => $transaction, $menu
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'customer_name' => ['required'],
            'type' => ['required'],
            // 'order' => array(
            //     'qty' => ['required']
            // )
        ]);

        $inputTransaction = $request->all();

        if ($validator->fails()) {
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        } try {
            $order = Order::create($request->all());

            $inputTransaction['order_id'] = $order->id;

            $transaction = Transaction::create($inputTransaction);

            // foreach ($request->order as $key => $value) {
            //     $order_detail = array(
            //         'order_id' => $order->id,
            //         'menu_id' => $value['menu_id'] , 
            //         'qty' => $value['qty'] , 
            //         'unit_price' => $value['unit_price'] , 
            //         'amount' => $value['amount'] , 
            //         'discount' => $value['discount']
            //     );

            //     $order_detail = OrderDetail::create($order_detail);
            // }
            for ($menu_id=0; $menu_id < count($request->menu_id); $menu_id++) { 
                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->menu_id = $request->menu_id[$menu_id];
                $order_detail->qty = $request->qty[$menu_id];
                $order_detail->unit_price = $request->unit_price[$menu_id];
                $order_detail->amount = $request->unit_price[$menu_id];
                $order_detail->discount = $request->unit_price[$menu_id];
                $order_detail->save();
            }
            $response = [
                'message' => 'Transaction created',
                'data' => $transaction, $order, $order_detail
            ];

            

            return response()->json($response, Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

       
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function show($id)
    {
        $transaction = Transaction::findOrFail($id);
        $response = [
            'message' => 'Transaction of resource details',
            'data' => $transaction
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'transaction_id' => ['required', 'numeric'],
            'qty' => ['required', 'numeric'],
            'amount' => ['required', 'numeric'],
            'total_price' => ['required', 'numeric'],
            'type' => ['required', 'in:offline,online']
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        } try {
            $transaction->update($request->all());
            $response = [
                'message' => 'Transaction updated',
                'data' => $transaction
            ];

            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $transaction = Transaction::findOrFail($id);

       try {
            $transaction->delete();
            $response = [
                'message' => 'Transaction deleted'
            ];

            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }
}
