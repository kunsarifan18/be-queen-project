<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $table = 'order_details';
    protected $fillable = [
    'order_id', 'menu_id', 'qty', 'unit_price', 'amount', 'discount'
    ];

    public function order_detail()
    {
        return $this->belongsTo("App\Models\Transaction");
    }

    public function order()
    {
        return $this->belongsTo("App\Models\Order");
    }
}
