<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = ['name','price', 'category', 'desc'];

    public function menu()
    {
        return $this->belongsTo("App\Models\OrderDetail");
    }
}
