<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = [ 'customer_name','driver_name'];

    public function transaction()
    {
        return $this->hasMany("App\Models\Transaction");
    }

    public function order()
    {
        return $this->hasMany("App\Models\OrderDetail");
    }

    public function order_detail()
    {
        return $this->belongsTo("App\Models\OrderDetail");
    }

    public function menu()
    {
        return $this->belongsTo("App\Models\Menu");
    }


}
